import pytest
from pages.cart_page import CartPage
from pages.product_page import ProductPage
from base.links import Links
from base.data_for_test import ExpectedData


class TestCartPage:
    @pytest.mark.parametrize("link, expected", [(link, expected) for link, expected in zip(Links.CART_PAGES, ExpectedData.EMPTY_CART_TEXT)])
    def test_text_for_empty_cart_is_visible(self, driver, link, expected):
        page = CartPage(driver, link)
        page.open()
        text = page.empty_cart_text_is_visible().text
        assert text == expected

    @pytest.mark.parametrize("link, expected", [(link, home_link) for link, home_link in zip(Links.CART_PAGES, Links.HOME_PAGES)])
    def test_link_in_text_for_empty_cart_is_visible(self, driver, link, expected):
        page = CartPage(driver, link)
        page.open()
        element = page.emty_cart_link_in_text_is_visible()
        href = element.get_attribute('href')
        assert href == expected

    @pytest.mark.parametrize("link", Links.PRODUCT_FOR_CART_TESTS)
    def test_mini_cart_amount_1(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.simple_is_visible().click()
        page.add_to_cart_button_is_visible().click()
        number = page.counter_number_in_cart_is_visible("1")
        assert number == True

    def test_mini_cart_amount_2(self, driver):
        page = ProductPage(driver, Links.PRODUCT_FOR_CART_TESTS[0])
        page.open()
        page.simple_is_visible().click()
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.add_to_cart_button_is_visible().click()
        number = page.counter_number_in_cart_is_visible("2")
        assert number == True

    @pytest.mark.parametrize("link", Links.PRODUCT_FOR_CART_TESTS)
    def test_basket_amount_1(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.simple_is_visible().click()
        page.add_to_cart_button_is_visible().click()
        page.cart_is_visible().click()
        page.view_basket_button_is_visible().click()
        page = CartPage(driver, driver.current_url)
        qty_value = page.qty_field_isvisible().get_attribute('value')
        assert qty_value == "1"

    def test_basket_amount_2(self, driver):
        page = ProductPage(driver, Links.PRODUCT_FOR_CART_TESTS[0])
        page.open()
        page.simple_is_visible().click()
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.add_to_cart_button_is_visible().click()
        page.cart_is_visible().click()
        page.view_basket_button_is_visible().click()
        page = CartPage(driver, driver.current_url)
        qty_value = page.qty_field_isvisible().get_attribute('value')
        assert qty_value == "2"

    @pytest.mark.parametrize("link", Links.PRODUCT_FOR_CART_TESTS)
    def test_count_minicart_price_1(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.simple_is_visible().click()
        price_1 = page.product_price_is_visible().text
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.cart_is_visible().click()
        price_2 = page.minicart_price_is_visible().text
        assert price_1 == price_2

    def test_count_minicart_price_2(self, driver):
        page = ProductPage(driver, Links.PRODUCT_FOR_CART_TESTS[0])
        page.open()
        page.simple_is_visible().click()
        price_1 = page.product_price_is_visible().text
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("2")
        page.cart_is_visible().click()
        price_2 = page.minicart_price_is_visible().text
        assert float(price_1[1:]) * 2 == float(price_2[1:])

    @pytest.mark.parametrize("link", Links.PRODUCT_FOR_CART_TESTS)
    def test_count_basket_price_1(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.simple_is_visible().click()
        price_1 = page.product_price_is_visible().text
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.cart_is_visible().click()
        page.view_basket_button_is_visible().click()
        page = CartPage(driver, driver.current_url)
        price_2 = page.subtotal_price_is_visible().text
        assert price_1 == price_2

    def test_count_basket_price_2(self, driver):
        page = ProductPage(driver, Links.PRODUCT_FOR_CART_TESTS[0])
        page.open()
        page.simple_is_visible().click()
        price_1 = page.product_price_is_visible().text
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("1")
        page.add_to_cart_button_is_visible().click()
        page.counter_number_in_cart_is_visible("2")
        page.cart_is_visible().click()
        page.view_basket_button_is_visible().click()
        page = CartPage(driver, driver.current_url)
        price_2 = page.subtotal_price_is_visible().text
        assert float(price_1[1:]) * 2 == float(price_2[1:])



