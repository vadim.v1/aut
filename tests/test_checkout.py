import pytest
from base.links import Links
from pages.checkout_page import CheckoutPage
from pages.product_page import ProductPage
from pages.header import Header
import time


class TestCheckout:

    @pytest.mark.smokes
    def test_checkout(self, driver):
        link = Links.PRODUCT_ONLY_FOR_CHECKOUT_TEST
        page = ProductPage(driver, link)
        page.open()
        page.first_option_is_visible_only_for_checkout_test().click()
        page.second_option_is_visible_only_for_checkout_test().click()
        page.add_to_cart_button_is_visible().click()
        page = Header(driver, link)
        page.cart_is_visible().click()
        page.go_to_chekout_button_is_visible().click()
        page = CheckoutPage(driver, link)
        page.email_field_is_visible().send_keys("andrii.l@interactivated.me")
        page.first_name_field_is_visible().send_keys("test")
        page.second_name_field_is_visible().send_keys("test")
        page.street_address_1st_field_is_visible().send_keys("test")
        page.dropdown_select("IT")
        page.city_field_is_visible().send_keys("test")
        page.post_code_field_is_visible().send_keys("11111")
        page.phone_number_field_is_visible().send_keys("0000000000")
        page.next_button_is_visible().click()
        page.cash_on_delivery_radio_is_visible().click()
        page.place_order_button_for_active_radio().click()
        assert "Your order # is: " in page.checkout_success_text_is_vivble().text


