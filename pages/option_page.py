from base.selenium_base import SeleniumBase
from base.locators import OptionPageLocators


class OptionPage(SeleniumBase):

    def first_option(self):
        first_option = self.is_visible(*OptionPageLocators.FIRST_OPTION)
        return first_option

    def continue_button(self):
        continue_button = self.is_visible(*OptionPageLocators.CONTINUE_BUTTON)
        return continue_button

    def link_for_stage(self):
        cur_url = self.driver.current_url
        cur_url = cur_url[:7] + "dev:dev@" + cur_url[9:]
        return cur_url

    def is_hreflag_present(self):
        hreflang_en = self.is_present(*OptionPageLocators.HREFLANG_EN)
        return hreflang_en

    def is_not_hreflang_present(self):
        try:
            assert self.is_element_present_bool(*OptionPageLocators.HREFLANG_EN)
            return False
        except AssertionError:
            return True

    def is_canonical_present(self):
        canonical = self.is_present(*OptionPageLocators.CANONICAL)
        return canonical

    def are_options_present(self):
        try:
            assert self.are_elements_present_bool(*OptionPageLocators.FIRST_OPTION)
            return True
        except AssertionError:
            return False

    def options_check(self, option_list):
        labels = self.are_present(*OptionPageLocators.FINAL_OPTIONS)
        counter = 0
        labels_text = []
        for num in labels:
            labels_text.append(num.text)
        for i in range(len(option_list)):
            for num in labels_text:
                if option_list[i] in num:
                    counter += 1
                    break
        return counter == len(option_list)





