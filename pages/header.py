from base.selenium_base import SeleniumBase
from base.locators import HeaderLocators


class Header(SeleniumBase):

    def cart_is_visible(self):
        return self.is_visible(*HeaderLocators.CART)

    def go_to_chekout_button_is_visible(self):
        return self.is_visible(*HeaderLocators.GO_TO_CHECKOUT_BUTTON)
