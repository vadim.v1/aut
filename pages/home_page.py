from base.data_for_test import ExpectedData
from base.selenium_base import SeleniumBase
from base.locators import HomePageLocators


class HomePage(SeleniumBase):

    def category_banners_h3_are_correct(self, current, expected):
        category_banners_h3 = self.are_visible(*current)
        for i in range(len(category_banners_h3)):
            assert category_banners_h3[i].text == expected[i]

    def category_banners_description_are_correct(self, current, expected):
        category_banners_description = self.are_visible(*current)
        for i in range(len(category_banners_description)):
            assert category_banners_description[i].text == expected[i]

    def model_selector_dropdown_list_values(self):
        x = self.is_visible(*HomePageLocators.MODEL_SELECT_DROPDOWN).text
        x = x.split("\n")
        return x

    def model_selector_dropdown_list_values_text(self, expected):
        return self.is_text_in_element_present(*HomePageLocators.MODEL_SELECT_DROPDOWN, expected)

    def vehicle_selector_dropdown_list_values_text(self, expected):
        return self.is_text_in_element_present(*HomePageLocators.VEHICLE_SELECT_DROPDOWN, expected)

    def shop_your_vehicle_is_visible(self):
        return self.is_visible(*HomePageLocators.SHOP_YOUR_VEHICLE_BUTTON)


