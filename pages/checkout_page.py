from base.selenium_base import SeleniumBase
from base.locators import CheckoutPageLocators
from selenium.webdriver.support.ui import Select


class CheckoutPage(SeleniumBase):

    def email_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.EMAIL_FIELD)

    def first_name_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.FIRST_NAME_FIELD)

    def second_name_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.SECOND_NAME_FIELD)

    def street_address_1st_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.STREET_ADDRESS_1ST_FIELD)

    def country_dropdown_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.COUNTRY_DROPDOWN)

    def city_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.CITY_FIELD)

    def post_code_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.POST_CODE_FIELD)

    def phone_number_field_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.PHONE_NUMBER_FIELD)

    def next_button_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.NEXT_BUTTON)

    def dropdown_select(self, value):
        return Select(self.is_visible(*CheckoutPageLocators.COUNTRY_DROPDOWN)).select_by_value(value)

    def cash_on_delivery_radio_is_visible(self):
        return self.is_visible(*CheckoutPageLocators.CASH_ON_DELIVERY_RADIO)

    def place_order_button_for_active_radio(self):
        return self.is_visible(*CheckoutPageLocators.PLACE_ORDER_BUTTON_FOR_ACTIVE_RADIO)

    def checkout_success_text_is_vivble(self):
        return self.is_visible(*CheckoutPageLocators.CHECKOUT_SUCCESS)

