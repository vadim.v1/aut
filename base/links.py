class Links:
    PRODUCT_WITH_OPTION_ENG = ["https://import.aut1.stage.interactivated.me/en/sunshades/jeep/cherokee/2014/",
                               "https://import.aut1.stage.interactivated.me/en/floorliner/tesla/model-x/2018/",
                               "https://import.aut1.stage.interactivated.me/en/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
                               "https://import.aut1.stage.interactivated.me/en/sunshades/bmw/3-series-(e36)/1999/",
                               "https://import.aut1.stage.interactivated.me/en/floorliner/bmw/3-series-(g20)/2021/",
                               "https://import.aut1.stage.interactivated.me/en/sunshades/bmw/4-series-(f32f33f36)/2020/"]
    PRODUCT_WITH_OPTION_NO_ENG = [
        "https://import.aut1.stage.interactivated.me/fr/floorliner/bmw/2-series-(f44)/2020/",
        "https://import.aut1.stage.interactivated.me/fr/sunshades/jeep/cherokee/2014/",
        "https://import.aut1.stage.interactivated.me/fr/floorliner/tesla/model-x/2018/",
        "https://import.aut1.stage.interactivated.me/fr/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/fr/sunshades/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/fr/floorliner/bmw/3-series-(g20)/2021/",
        "https://import.aut1.stage.interactivated.me/fr/sunshades/bmw/4-series-(f32f33f36)/2020/",
        "https://import.aut1.stage.interactivated.me/de/floorliner/bmw/2-series-(f44)/2020/",
        "https://import.aut1.stage.interactivated.me/de/sunshades/jeep/cherokee/2014/",
        "https://import.aut1.stage.interactivated.me/de/floorliner/tesla/model-x/2018/",
        "https://import.aut1.stage.interactivated.me/de/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/de/sunshades/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/de/floorliner/bmw/3-series-(g20)/2021/",
        "https://import.aut1.stage.interactivated.me/de/sunshades/bmw/4-series-(f32f33f36)/2020/",
        "https://import.aut1.stage.interactivated.me/it/floorliner/bmw/2-series-(f44)/2020/",
        "https://import.aut1.stage.interactivated.me/it/sunshades/jeep/cherokee/2014/",
        "https://import.aut1.stage.interactivated.me/it/floorliner/tesla/model-x/2018/",
        "https://import.aut1.stage.interactivated.me/it/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/it/sunshades/bmw/3-series-(e36)/1999/",
        "https://import.aut1.stage.interactivated.me/it/floorliner/bmw/3-series-(g20)/2021/",
        "https://import.aut1.stage.interactivated.me/it/sunshades/bmw/4-series-(f32f33f36)/2020/"
    ]
    LOGIN_URL = "https://import.aut1.stage.interactivated.me/en/customer/account/login/"
    MY_ACCOUNT_PAGE_INDEX = "https://import.aut1.stage.interactivated.me/en/customer/account/index/"
    REGISTRATION_PAGE = "https://import.aut1.stage.interactivated.me/en/customer/account/create/"
    MY_ACCOUNT_PAGE = "https://import.aut1.stage.interactivated.me/en/customer/account/"
    HOME_PAGE_EN = "https://import.aut1.stage.interactivated.me/en/"
    HOME_PAGES = [
        "https://import.aut1.stage.interactivated.me/en/",
        "https://import.aut1.stage.interactivated.me/fr/",
        "https://import.aut1.stage.interactivated.me/de/",
        "https://import.aut1.stage.interactivated.me/it/"]
    CART_PAGES = [
        "https://import.aut1.stage.interactivated.me/en/checkout/cart/",
        "https://import.aut1.stage.interactivated.me/fr/checkout/cart/",
        "https://import.aut1.stage.interactivated.me/de/checkout/cart/",
        "https://import.aut1.stage.interactivated.me/it/checkout/cart/"]
    PRODUCT_FOR_CART_TESTS = ["https://import.aut1.stage.interactivated.me/en/phone-tablet-accessories/cupfone-extension/",
                              "https://import.aut1.stage.interactivated.me/fr/accessoires-pour-telephone-tablettes/cupfone-extension/",
                              "https://import.aut1.stage.interactivated.me/de/telefon-tablet-zubehor/cupfone-extension/",
                              "https://import.aut1.stage.interactivated.me/it/accessori-cellulari-e-tablet/cupfone-extension/"]
    PRODUCTS_NO_OPTIONS_EN = ["https://import.aut1.stage.interactivated.me/en/side-window-deflector/kia/soul-ev/2019/",
                           "https://import.aut1.stage.interactivated.me/en/seat-protector/land-rover--range-rover/discovery-series-ii/2005/",
                           "https://import.aut1.stage.interactivated.me/en/sunshades/mazda/cx-5/2016/",
                           "https://import.aut1.stage.interactivated.me/en/seat-protector/land-rover--range-rover/freelander/2006/",
                           "https://import.aut1.stage.interactivated.me/en/side-window-deflector/land-rover--range-rover/freelander--freelander-2--lr2/2015/"]
    PRODUCTS_NO_OPTIONS_IT = ["https://import.aut1.stage.interactivated.me/it/side-window-deflector/kia/soul-ev/2019/",
                           "https://import.aut1.stage.interactivated.me/it/sunshades/mazda/cx-5/2016/",
                           "https://import.aut1.stage.interactivated.me/it/seat-protector/land-rover--range-rover/freelander/2006/",
                           "https://import.aut1.stage.interactivated.me/it/side-window-deflector/land-rover--range-rover/freelander--freelander-2--lr2/2015/",
                           "https://import.aut1.stage.interactivated.me/it/altri-prodotti/gift-bags/"]
    CATEGORIES_IT = ["https://import.aut1.stage.interactivated.me/it/prodotti-per-casa-ufficio/",
                     "https://import.aut1.stage.interactivated.me/it/altri-prodotti/",
                     "https://import.aut1.stage.interactivated.me/it/accessori-cellulari-e-tablet/"]
    PRODUCT_ONLY_FOR_CHECKOUT_TEST = "https://import.aut1.stage.interactivated.me/en/home-business-products/coasters/"
