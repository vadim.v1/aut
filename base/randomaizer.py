import random
import string


class Randomizer:
    @staticmethod
    def random_char(char_num):
        return ''.join(random.choice(string.ascii_letters) for _ in range(char_num))

    @staticmethod
    def random_option(options_list):
        return random.randint(0, len(options_list) - 1)
